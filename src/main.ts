import jQuery from 'jquery';
import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import ToastMessageApp from './ToastMessageApp.vue';
import VueI18n from 'vue-i18n';
import { LanguageUtil } from '@coscine/app-util';

import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueI18n);

jQuery(() => {
    const i18n = new VueI18n({
      locale: LanguageUtil.getLanguage(),
      messages: coscine.i18n.toastmessage,
      silentFallbackWarn: true,
    });

    new Vue({
      render: (h) => h(ToastMessageApp),
      i18n,
    }).$mount('toastmessage');
});
